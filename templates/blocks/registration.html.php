<?php if (!empty($message)) {echo "<p class=\"error\">" . "MESSAGE: ". $message . "</p>";} ?>


<div class="container mlogin">
    <div id="login">
        <h1>REGISTRATION</h1>
        <form name="registerform" id="registerform" action="registration.php" method="post" >

            <p>
                <label for="name">Name <font color="red">*</font> ( field is required )
                    <br />
                    <input type="text" name="name" id="name" class="input" size="32" value="" placeholder="NAME" />
                </label>
            </p>

            <p>
                <label for="full_name">Surname <font color="red">*</font> ( field is required )
                    <br />
                    <input type="text" name="full_name" id="full_name" class="input" size="32" value="" placeholder="SURNAME" />
                </label>
            </p>


            <p>
                <label for="email">Email <font color="red">*</font> ( field is required )
                    <br />
                    <input type="email" name="email" id="email" class="input" value="" size="32" placeholder="EMAIL" required/>
                </label>
            </p>

            <p>
                <label for="phone">Mobile phone number <font color="red">*</font> ( field is required )
                    <br />
                    <input type="text" name="phone" id="phone" class="input" value="" size="20" placeholder="PHONE NUMBER" />
                </label>
            </p>

            <p>
                <label for="contacts">Other contacts
                    <br />
                    <input type="text" name="contacts" id="contacts" class="input" value="" size="30" placeholder="OTHER CONTACTS" />
                </label>
            </p>

            <p>
                <label for="user_pass">Password <font color="red">*</font> ( field is required )
                    <br />
                    <input type="password" name="password" id="password" class="password" value="" size="32" placeholder="PASSWORD" required/>
                </label>

            </p>
            <p>
                <label for="sum">Choose sum which you want to invest

                  <select class="input" name="sum" id="sum" >
                      <option selected disabled>SELECT</option>
                      <option value="Up to 1 000$">Up to 1 000$</option>
                      <option value="1 000$ - 3 000$">1 000$ - 3 000$</option>
                      <option value="3 000$ - 5 000$">3 000$ - 5 000$</option>
                      <option value="5 000$ - 10 000$">5 000$ - 10 000$</option>
                      <option value="over 10 000$">Over 10 000$</option>
                  </select>

              </label>
            </p>

            <p>
                <label for="feedback">Where did you hear about us?
                      <select class="input" name="sum" id="sum" >
                          <option selected disabled>SELECT</option>
                          <option value="FACEBOOK">FACEBOOK</option>
                          <option value="TWITTER">TWITTER</option>
                          <option value="INSTARGAM">INSTARGAM</option>
                          <option value="Other source">Other source</option>
                      </select>
                </label>
            </p>

            <p class="submit">
                <input type="submit" name="register" id="register" class="button" value="Register" />
            </p>

            <p class="regtext">Already have an account? <a href="login.php">Login Here</a>!</p>

        </form>

    </div>
</div>
