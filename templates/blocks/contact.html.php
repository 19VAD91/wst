


<div id="profile">
  <div class="mssgCon">
  <form method="POST" id="feedback-form">
  <p class="contact">Fill the form</p>
  <input type="text" name="nameFF" class="input" required placeholder="Name" x-autocompletetype="name">

  <input type="email" name="contactFF" class="input" required placeholder="E-mail" x-autocompletetype="email">

  <textarea class="input" name="messageFF" placeholder="Message" required rows="5"></textarea>
  <input type="submit" class="button" value="Send">
  </form>
</div>
</div>



<script>
document.getElementById('feedback-form').addEventListener('submit', function(evt){
  var http = new XMLHttpRequest(), f = this;
  evt.preventDefault();
  http.open("POST", "feedbackMail.php", true);
  http.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  http.send("nameFF=" + f.nameFF.value + "&contactFF=" + f.contactFF.value + "&messageFF=" + f.messageFF.value);
  http.onreadystatechange = function() {
    if (http.readyState == 4 && http.status == 200) {
      $(".message").html('Your message has been sent.\nOur experts will respond to you within 2 days.\n');
      f.messageFF.removeAttribute('value'); // очистить поле сообщения (две строки)
      f.messageFF.value='';
    }
  }
  http.onerror = function() {
    $(".message").html('Something went wrong. Please contact to us via skype.');
  }
}, false);

</script>
