<?php
require_once('includes/bootstrap.php');


if(isset($_POST["edit"])){
    if (!empty($_POST['full_name']) && !empty($_POST['email']) && !empty($_POST['phone']) && !empty($_POST['password']) && !empty($_POST['name']) && !empty($_POST['contacts'])) {
        $full_name = !empty($_POST['full_name']) ? trim(strip_tags($_POST['full_name'])) : "";
        $password = !empty($_POST['password']) ? trim(strip_tags($_POST['password'])) : "";
        $email=!empty($_POST['email']) ? trim(strip_tags($_POST['email'])) : "";
        $phone=!empty($_POST['phone']) ? trim(strip_tags($_POST['phone'])) : "";
        $name =!empty($_POST['name']) ? trim(strip_tags($_POST['name'])) : "";
        $contacts=!empty($_POST['contacts']) ? trim(strip_tags($_POST['contacts'])) : "";

        $result = updateUser($full_name, $email, $phone, $password, $name, $contacts);


        if($result==true){



            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset="windows-1251"' . "\r\n";
            $headers .= 'From: support@wealthshiptrade.com'. "\r\n";
            mail($email, "Registration", "Your info was update.", $headers);
            if($result){
               $message = "Account successfully updated.";
               $_SESSION['session_email']=$email;
               $_SESSION['name']=$name;

            } else {
                $message = "Failed to insert data information!";
            }

        } else {
           $message = "That email already exists! Please try another one!";
        }
    } else {
	 $message = "All fields are required!";
}
}



echo loadTemplate('layots/base',
	[
		'pageTitle' => 'edit',

      'message' => @$message,
	]
);
