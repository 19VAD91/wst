<?php


/*
*Главная (индексная) страница
*/
require_once('includes/bootstrap.php');

if(isset($_SESSION['session_email'])){
    // echo "Session is set"; // for testing purposes
    header("Location: intropage.php");
}

if(isset($_POST["register"])){
    if (!empty($_POST['full_name']) && !empty($_POST['email']) && !empty($_POST['phone']) && !empty($_POST['password']) && !empty($_POST['feedback']) && !empty($_POST['name']) && !empty($_POST['contacts'])) {
        $full_name = !empty($_POST['full_name']) ? trim(strip_tags($_POST['full_name'])) : "";
        $password = !empty($_POST['password']) ? trim(strip_tags($_POST['password'])) : "";
        $email=!empty($_POST['email']) ? trim(strip_tags($_POST['email'])) : "";
        $phone=!empty($_POST['phone']) ? trim(strip_tags($_POST['phone'])) : "";
        $sum =!empty($_POST['sum']) ? trim(strip_tags($_POST['sum'])) : "";
        $feedback=!empty($_POST['feedback']) ? trim(strip_tags($_POST['feedback'])) : "";
        $name =!empty($_POST['name']) ? trim(strip_tags($_POST['name'])) : "";
        $contacts=!empty($_POST['contacts']) ? trim(strip_tags($_POST['contacts'])) : "";
        $numrows = getDBEmail($email);


        if($numrows==null){
            $activation = getActivationLink($email);
            $result = createNewUser($full_name, $email, $phone, $password, $sum, $feedback, $name, $contacts, $activation);

            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset="windows-1251"' . "\r\n";
            $headers .= 'From: support@wealthshiptrade.com'. "\r\n";
            mail($email, "Registration", "Thank you for registering. Please confirm your account to complete the registration process by visiting the link <a href='http://wealthshiptrade.com/act.php?email=$email&key=$activation'> http://wealthshiptrade.com/act.php?email=$email&key=$activation </a> ", $headers);
            if($result){
               $message = "Account Successfully Created. Please check your email and click the confirmation link.";

            } else {
                $message = "Failed to insert data information!";
            }

        } else {
           $message = "That email already exists! Please try another one!";
        }
    } else {
	 $message = "All fields are required!";
}
}

echo loadTemplate('layots/base',

	[

		'pageTitle' => 'registration',

        'message' => @$message,

	]

);
