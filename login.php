<?php


require_once('includes/bootstrap.php');
    // if(@$_SESSION['name']){
    //     unset($_SESSION['session_email']);
    //     session_destroy();
    // }

    if(isset($_SESSION['session_email'])){
        // echo "Session is set"; // for testing purposes
        header("Location: intropage.php");
    }

    if(isset($_POST["login"])){
        if(!empty($_POST['email']) && !empty($_POST['password'])) {

                $email=$_POST['email'];
                $password=$_POST['password'];
                $numrows=getCheckPassword($email, $password);



                if($numrows){
                    $dbemail = $numrows[0]['email'];
                    $dbpassword = $numrows[0]['password'];
                    $name = $numrows[0]['name'];
                    $id = $numrows[0]['id'];

                    if($email == $dbemail && $password == $dbpassword){

                        $_SESSION['session_email']=$email;
                        $_SESSION['name']=$name;
                        $_SESSION['id']=$id;
                        /* Redirect browser */

                        header("Location: intropage.php");

                    }

                } else {

                    $message =  "Invalid email or password!";

                }

    } else {

        $message = "All fields are required!";

    }

    }


echo loadTemplate('layots/base',
	[
		'pageTitle' => 'login',
        'message' => @$message,
    ]
);
