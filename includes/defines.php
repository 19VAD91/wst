<?php
/*
* Константы и переменные, используемые в проекте.
*/

define('ROOT_DIR', str_replace('\\', '/', dirname(__DIR__))); //путь до корневого каталога веб сервера
define('TEMPLATES_DIR', ROOT_DIR . '/templates'); //путь до каталога с шаблонами
define('MODELS_DIR', ROOT_DIR . '/models'); //путь до каталога с данными моделями
