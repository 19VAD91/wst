<div class="about">
  <p>
<ol>
<li><font class="question">What are trading signals?</font><br>
<font class="answer">Trading signals are short messages that contain information about the best transactions with any financial instruments.</font>
</li>
<li><font class="question">How to trade with the signals?</font><br>
<font class="answer">You get a trading signal for the chosen trading platform and financial instrument. Take a decision about making a transaction. Place an order of buying or selling.</font>
</li>
<li><font class="question">Why it is better to use the signals than try to predict?</font><br>
<font class="answer">At the time of creation the signals, we use patterns, which serving experience of professional traders, exchange analysts. The probability of making successful transaction is very high.</font>
</li>
<li><font class="question">How to pay for the signals?</font><br>
<font class="answer">Make payments using a variety of methods including:<br>
Credit card, Wire transfer, Bitcoin. The payment takes from a few minutes up to a few days, it depends on payment methods.</font>
</li>
<li><font class="question">How to get the signal?</font><br>
<font class="answer">Signals are sent on the phone as SMS, or on any messenger, or on any email.</font>
</li>
<li><font class="question">How much time does the signal work?</font><br>
<font class="answer">From a few minutes up to a few days. It depends on selected market, time interval, financial instruments.</font>
</li>
<li><font class="question">Do you have test access?</font><br>
<font class="answer">No, we don’t. We don’t have free test access. Information has a price.</font>
</li>
<li><font class="question">How often are signals sent?</font><br>
<font class="answer">It depends on chosen financial instrument and situation on the market. From 1-2 signals a day.</font>
</li>
<li><font class="question">How soon will I get signals after payment of subscription?</font><br>
<font class="answer">It  depends on selected market, time interval, financial instruments.</font>
</li>
<li><font class="question">What average profit can I get using your signals?</font><br>
<font class="answer">You can get about 20% a month, using our signals, but often when you make transactions, the profit will grow up to 35% from one transaction.</font>
</li>
<li><font class="question">How to choose a broker?</font><br>
<font class="answer">Choosing a broker it is not an easy process. It is necessary to take an account his reputation, experience, references. We can take care of that. We will place your investment profitably.</font>
</li>
<li><font class="question">What financial instruments will I get signals on?</font><br>
<font class="answer">When you subscribe, you will get signals for Hong Kong exchanges.</font>
</li>
<li><font class="question">Can I get signals on the instruments what I need?</font><br>
<font class="answer">Yes, you can. You can choose the instruments, but the rate will be individual.</font>
</li>
<li><font class="question">Do you only have paid subscription?</font><br>
<font class="answer">Yes, we do. We only have paid subscription.</font>
</li>
</ol>
  </P>
<div>
