<header>
  <div class="logo">
    <a href="index.php"><img src="img/logo1.png"></a>

  </div>

  <div class="top">

    <div class="navbar">
      <ul>
        <li>
          <a class="padding-left-0" href="index.php">HOME</a>
        </li>
        <li>
          <a class="padding-left-0" href="/profile.php">PROFILE</a>
        </li>
        <li>
          <a class="padding-left-0" href="/about.php">ABOUT US</a>
        </li>
        <li>
          <a class="padding-left-0" href="/howitworks.php">HOW IT WORKS</a>
        </li>
        <li>
          <a class="padding-left-0" href="/faq.php">FAQ</a>
        </li>
        <li>
          <a class="padding-left-0" href="/contact.php">CONTACT</a>
        </li>
      </ul>
    </div>

  </div>
  <?php if (isset($_SESSION['session_email']) && !empty($_SESSION['session_email'])) { ?>
    <div class="top2">
      Hello, <a href="profile.php"><?php echo @$_SESSION['name'] ?></a>

    </div>
  <?php } ?>
<?php if (!isset($_SESSION['session_email']) && empty($_SESSION['session_email'])) { ?>
    <div class="top2">
      <a href="login.php">Login</a>
      <a href="registration.php">Registration</a>
    </div>
  <?php } ?>
</header>
