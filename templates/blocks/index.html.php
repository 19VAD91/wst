<div class="content">
  <div class="con">
  <div class="con1">
    <p>
      <h2>Precision predictions for private traders and investors</h2>

Private traders utilize these daily forecasts as a tool to enhance portfolio performance, verify their own analysis and act faster on market opportunities.

<h3>How it works</h3>

We provide highly accurate signals about the situation on the stock exchange. To receive them you only need to <a href="registration.php">register</a> and buy a package that suits you.<br><br>
Now we have the following preposal:<br><br>

One signal &nbsp;&nbsp;- cost 45$<br>
Five signals - cost 225$<br>
Ten signals &nbsp;- cost 450$<br>

<h4> What is signal</h4>
A signal is a message with the forecast that we will send you as soon as we have an accurate prediction of the future market situation and recommendations on operations.
    </p>
  </div>
  <div class="con2">
    <p>
      <a class="twitter-timeline" height="480px" data-chrome="nofooter noborders transparent noscrollbar" href="https://twitter.com/Wealth_Ship" data-widget-id="722905243931906049">Tweets by @Wealth_Ship</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

    </p>
  </div>
</div>
</div>
