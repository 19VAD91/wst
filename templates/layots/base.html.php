<!DOCTYPE html>
<html lang="ru">
    <head>
        <!--Здесь head-->
        <?= loadTemplate('blocks/head')?>
    </head>
    <body>
        <!--Здесь header-->
        <?= loadTemplate('blocks/header')?>
        <!--Здесь форма-->
        <?if($pageTitle == "index"):?>
            <?= loadTemplate('blocks/index')?>
        <?elseif($pageTitle == "about"):?>
            <?= loadTemplate('blocks/about')?>
        <?elseif($pageTitle == "registration"):?>
            <?= loadTemplate('blocks/registration')?>
        <?elseif($pageTitle == 'restore'):?>
            <?= loadTemplate('blocks/restore')?>
        <?elseif($pageTitle == 'login'):?>
            <?= loadTemplate('blocks/login')?>
        <?elseif($pageTitle == 'hello'):?>
            <?= loadTemplate('blocks/hello')?>
        <?elseif($pageTitle == 'profile'):?>
            <?= loadTemplate('blocks/profile')?>
        <?elseif($pageTitle == 'howitworks'):?>
            <?= loadTemplate('blocks/howitworks')?>
        <?elseif($pageTitle == 'faq'):?>
            <?= loadTemplate('blocks/faq')?>
        <?elseif($pageTitle == 'contact'):?>
            <?= loadTemplate('blocks/contact')?>
        <?elseif($pageTitle == 'thanks'):?>
            <?= loadTemplate('blocks/thanks')?>
        <?elseif($pageTitle == 'edit'):?>
            <?= loadTemplate('blocks/edit')?>
        <?elseif($pageTitle == 'mssg'):?>
            <?= loadTemplate('blocks/mssg')?>
        <?elseif($pageTitle == 'admin'):?>
            <?= loadTemplate('blocks/admin')?>
        <?elseif($pageTitle == 'sendMssg'):?>
            <?= loadTemplate('blocks/sendMssg')?>
        <?elseif($pageTitle == 'usersInfo'):?>
            <?= loadTemplate('blocks/usersInfo')?>
        <?elseif($pageTitle == 'support'):?>
            <?= loadTemplate('blocks/support')?>                  
        <?endif?>
        <!--Здесь будет footer-->
        <?= loadTemplate('blocks/footer')?>
    </body>
</html>
