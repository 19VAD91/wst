<?php
/*
*
* Модель для вспомогательных функций.
*
*/
/*
* Выводит на экран переменную в человекочитаемом формате
*/
function dump($var)
{
    ob_start();
	var_dump($var);
    $dump = ob_get_contents();
    ob_end_clean();

    ob_start();
    highlight_string("<?php\n" . $dump . "\n?>");
    $highlightedDump = ob_get_contents();
    ob_end_clean();

    echo '<pre><strong><div style="padding:10px;background: rgba(204, 204, 204, 0.16);border: 1px dashed black;display: inline-block;">',
    str_replace(['&lt;?php', '?&gt;'], '', trim($highlightedDump)),
    '</div></strong></pre>';
}

/*
*Перенаправляет (редиректит) браузер на определенный URL
*/
function redirectTo($url){
	header('Location:' . $url);
	die();
}
/*
*Подключение файлов с попощью буфиризации
*/
function loadTemplate($_templateName, $data = [])
{
    static $_data = null;

    ob_start();

    if (is_null($_data)) {
        $_data = $data;
    }

    extract($_data, EXTR_OVERWRITE);
    $_templateFile = TEMPLATES_DIR . '/' . $_templateName . '.html.php';

    if (is_file($_templateFile) && is_readable($_templateFile)) {
        include($_templateFile);
    }

    $buffer = ob_get_contents();
    ob_end_clean();

    return $buffer;
}

/*
* Возвращает список файлов в заданной директории
*/
function getFilesInDirectory($dirPath)
{
    $files = scandir($dirPath);
    $_files = [];

    if (is_array($files) && count($files) > 0) {
        foreach ($files as $file) {
            if (is_file(IMG_PHOTO . '/' . $file)) {
                $_files[] = $file;
            }
        }
    }

    return $_files;
}
?>
